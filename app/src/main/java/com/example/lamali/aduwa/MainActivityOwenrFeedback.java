package com.example.lamali.aduwa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivityOwenrFeedback extends AppCompatActivity {
    private Button b1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_owenr_feedback);
        b1=(Button)findViewById(R.id.btnExperiment);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivityStaffOwnerStudent();
            }
        });


    }
    public void openMainActivityStaffOwnerStudent(){
        Intent intent =new Intent(this,MainActivityStaffOwnerStudent.class);
        startActivity(intent);
    }
}

