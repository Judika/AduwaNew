package com.example.lamali.aduwa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivityStaffOwnerStudent extends AppCompatActivity {
    private Button button1,button2;
    private Button button3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_staff_owner_student);


        button1=(Button)findViewById(R.id.btnOwner);
        button2=(Button)findViewById(R.id.btnStudent);
        button3=(Button)findViewById(R.id.btnStaff);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivityOwnerSingUp();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivityStudentApplication();
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStaffSingUpActivity();
            }
        });


    }
    public void openMainActivityOwnerSingUp(){
        Intent intent1=new Intent(this, MainActivityOwnerSingUp.class);
        startActivity(intent1);

    }
    public void openMainActivityStudentApplication(){
        Intent intent2=new Intent(this, MainActivityStudentApplication.class);
        startActivity(intent2);

    }

    public void openStaffSingUpActivity(){
        Intent intent3=new Intent(this, StaffSingUpActivity.class);
        startActivity(intent3);
    }

}

