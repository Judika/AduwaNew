package com.example.lamali.aduwa;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SelectCanteenActivity extends AppCompatActivity {
    private Button button1,button2,button3,button4,button5,button6;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_canteen);

        button1=(Button)findViewById(R.id.btnCanteen1);
        button2=(Button)findViewById(R.id.btnCanteen2);
        button3=(Button)findViewById(R.id.btnCanteen3);
        button4=(Button)findViewById(R.id.btnCanteen4);
        button5=(Button)findViewById(R.id.btnCanteen5);
        button6=(Button)findViewById(R.id.btnCanteen6);


        button1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                openDisplayMenu();
            }
        });
        button2.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                openDisplayMenu();
            }
        });
        button3.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                openDisplayMenu();
            }
        });
        button4.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                openDisplayMenu();
            }
        });

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDisplayMenu();
            }
        });
        button6.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                openDisplayMenu();
            }
        });
    }


    public void openDisplayMenu(){
        Intent intent1=new Intent(this,MainActivityDisplayMenu.class);
        startActivity(intent1);

    }


}
